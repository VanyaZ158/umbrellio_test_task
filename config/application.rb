require_relative 'boot'

require 'rails/all'
require './lib/middleware/consider_all_request_json_middleware'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Umbrellio
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    config.autoload_paths << Rails.root.join('lib')
    config.autoload_paths << Rails.root.join('lib', 'services')

    config.middleware.insert_before(ActionDispatch::Static, ConsiderAllRequestJsonMiddleware)

    config.default_locale = :en
  end
end
