# README
## Umbrellio Test Tasks
### Rails
* Ruby 2.5.1
* Rails 5.2.1
* PostgreSQL 10.5
* Friendship is Magic

### SQL Query
```sql
SELECT MIN(id) as min_id, group_id, COUNT(*) FROM (
  SELECT ROW_NUMBER() OVER () - ROW_NUMBER() OVER (PARTITION BY group_id) AS grp, id, group_id FROM users
) AS tbl
  GROUP BY group_id, grp
  ORDER BY min_id
```
