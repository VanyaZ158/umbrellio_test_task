FactoryBot.define do
  factory :ip do
    value { FFaker::Internet.unique.ip_v4_address }
    users { [create(:user)] }
  end
end
