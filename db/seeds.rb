ips = Array.new(50) { FFaker::Internet.unique.ip_v4_address }
usernames = Array.new(100) { FFaker::Internet.unique.user_name }

2_000.times do
  ActiveRecord::Base.transaction do
    form = PostCreateService.call(
      title: FFaker::Book.title,
      body: 'Body',
      author_ip: ips.sample,
      username: usernames.sample
    )

    FactoryBot.create_list(:rating, 10, post: form.model)
  end
end
