class CreateIps < ActiveRecord::Migration[5.2]
  def change
    create_table :ips do |t|
      t.text :value, null: false
    end

    add_index :ips, :value, unique: true
  end
end
