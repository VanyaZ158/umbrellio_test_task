class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.text :title, null: false
      t.text :body, null: false
      t.text :author_ip, null: false
      t.belongs_to :user, index: true

      t.timestamps
    end

    add_index :posts, :title
    add_index :posts, :author_ip
  end
end
