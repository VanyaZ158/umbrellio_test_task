class CreateRatings < ActiveRecord::Migration[5.2]
  def change
    create_table :ratings do |t|
      t.integer :value, null: false
      t.belongs_to :post, index: true

      t.timestamps
    end
  end
end
