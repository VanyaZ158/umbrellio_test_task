class PostForm < ApplicationForm
  define_model :post

  attribute :title, String
  attribute :body, String
  attribute :user_id, Integer
  attribute :author_ip, String

  validates :title, :body, :user_id, :author_ip, presence: true, unless: :persisted?
  validates :title, :body, :author_ip, length: { minimum: 1 }, allow_nil: true
  validates :user_id, numericality: { only_integer: true }, allow_nil: true
end
