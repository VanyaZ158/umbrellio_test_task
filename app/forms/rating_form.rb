class RatingForm < ApplicationForm
  define_model :rating

  attribute :value, Integer
  attribute :post_id, Integer

  validates :value, :post_id, presence: true, unless: :persisted?
  validates :value, :post_id, numericality: { only_integer: true }, allow_nil: true
  validates :value, numericality: { less_than_or_equal_to: 5, greater_than_or_equal_to: 0 }, allow_nil: true
end
