class IpForm < ApplicationForm
  define_model :ip

  attribute :value, String

  def value=(value)
    @value = value
    @model = model.class.find_by(value: value) || Ip.new unless persisted?
  end
end
