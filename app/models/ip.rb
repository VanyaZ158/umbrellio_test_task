class Ip < ApplicationRecord
  has_and_belongs_to_many :users

  def self.with_many_users
    joins(:users)
      .select('ips.value as ip, array_agg(users.username) as usernames')
      .group('ips.value')
      .having('count(*) > 1')
  end
end
