class Rating < ApplicationRecord
  belongs_to :post

  def self.average_rating_for_post(post_id)
    joins(:post).where('posts.id = ?', post_id).average('ratings.value') || 0.0
  end
end
