class Post < ApplicationRecord
  belongs_to :user
  has_many :ratings, dependent: :destroy

  def self.top_by_rating(number_of_posts)
    left_joins(:ratings)
      .select('posts.id, posts.title, posts.body, COALESCE(AVG(ratings.value), 0) AS avg')
      .group('posts.id, posts.title, posts.body')
      .order('avg DESC, id')
      .limit(number_of_posts)
  end
end
