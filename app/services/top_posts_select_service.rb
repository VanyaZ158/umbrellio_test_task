class TopPostsSelectService < ApplicationService
  DEFAULT_NUMBER_OF_POSTS = 10

  include Virtus.model
  include ActiveModel::Model

  attribute :number_of_posts, Integer
  attribute :posts, Array, default: []

  validates :number_of_posts, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  def initialize(number_of_posts)
    super(number_of_posts: number_of_posts || DEFAULT_NUMBER_OF_POSTS)
  end

  def call
    return self unless valid?

    self.posts = Post.top_by_rating(number_of_posts)
    self
  end
end
