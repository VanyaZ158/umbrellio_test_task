module Api
  class UsersController < ApplicationController
    def index
      render json: Ip.with_many_users.to_json(except: :id)
    end
  end
end
