module Api
  class PostsController < ApplicationController
    def create
      form = PostCreateService.call(post_params.to_h)

      if form.valid?
        render json: form.model, status: :created
      else
        render json: form.errors, status: :unprocessable_entity
      end
    end

    def top
      result = TopPostsSelectService.call(params[:number_of_posts])

      if result.valid?
        render json: result.posts.to_json(only: %i[title body]), status: :ok
      else
        render json: result.errors, status: :bad_request
      end
    end

    private

    def post_params
      params.require(:post).permit(:title, :body, :author_ip, :username)
    end
  end
end
